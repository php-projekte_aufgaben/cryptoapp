const url = "http://localhost/php43_angabe/server/api/";

const app = Vue.createApp({
    data() {
        return {
            prices: [],
            assocp:[],
            wallets: [],
            purchases: []
        }
    },
    // Ausgeführt beim Start
    mounted() {
        this.loadPricesAssoc()
        this.loadPrices()
        this.loadWallets()
        this.loadPurchases()
    },
    methods: {
        loadPrices() {
            axios.get("https://api.bitpanda.com/v1/ticker").then(
                (response) => {
                    let prices =[];
                    let keys = Object.keys(response.data);
                    let values = Object.values(response.data);
                    let europrices = [];
                    values.forEach( value => {
                            europrices.push(value.EUR)
                        }
                    )

                    for(let i = 0; i < keys.length; i++){
                        let test = {currencies: keys[i], values: europrices[i]};
                        prices.push(test);
                    }
                    this.prices = prices;
                })

        },
        loadPricesAssoc() {
            axios.get("https://api.bitpanda.com/v1/ticker").then(response => {
                let prices = [];
                for (const [currency, value] of Object.entries(response.data)) {
                    prices[currency] = parseFloat(value.EUR)
                }
                this.assocp = prices
                //console.log(this.assocp['ETH']);
            })

        },
         loadWallets() {
            axios.get(url+"wallet",).then(response => {
                this.wallets = response.data
            })
        },
        loadPurchases(){
            axios.get(url+"purchase",).then(response => {
                this.purchases = response.data
            })
        },

        addPurchase(purchase) {
            //Purchase in Datenbank schreiben
            axios.post(url+"purchase", purchase).then(function (response) {
                console.log(response)
            }).finally(this.loadWallets)  // Kurzfristig sequenziell (Zuerst POST und dann loadWallets!)
                .catch(function (error) {
                    console.log(error)
                })
            a.loadPricesAssoc();
        },

        addWallet(wallet) {
            //Wallet in Datenbank schreiben
            axios.post(url+"wallet", wallet).then(function (response) {
                console.log(response)
            }).finally(this.loadWallets)  // Kurzfristig sequenziell (Zuerst POST und dann loadWallets!)
                .catch(function (error) {
                    console.log(error)
                })
        }
    },
})
