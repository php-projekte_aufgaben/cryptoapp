<?php

require_once('RESTController.php');
require_once('models/Wallet.php');
require_once ('models/WalletDetail.php');

class WalletRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }


    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            $model = WalletDetail::get($this->args[0]);
            $this->response($model);
        } else if ($this->verb == null && empty($this->args)) {
            $model = WalletDetail::getAll();
            $this->response($model);
        }
        else {
            $this->response("Bad request", 400);
        }
    }

    private function handlePOSTRequest()
    {
        $model = new Wallet();
        $model->setName($this->getDataOrNull('name'));
        $model->setCurrency($this->getDataOrNull('currency'));

        if ($model->save()) {
            $this->response("OK", 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }


    private function handlePUTRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {

            $model = Wallet::get($this->args[0]);
            $model->setName($this->getDataOrNull('name'));
            $model->setCurrency($this->getDataOrNull('currency'));

            if ($model->save()) {
                $this->response("OK");
            } else {
                $this->response($model->getErrors(), 400);
            }

        } else {
            $this->response("Not Found", 404);
        }
    }

    private function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Wallet::delete($this->args[0]);
            $this->response("OK", 200);
        } else {
            $this->response("Not Found", 404);
        }
    }

}
