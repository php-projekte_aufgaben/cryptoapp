<?php

require_once "models/WalletDetail.php";
require_once "models/Purchase.php";
require_once "models/Wallet.php";

class WalletDetail extends Wallet implements JsonSerializable
{
    private $amount;
    private $price;

    public function jsonSerialize()
    {
        $obj = parent::jsonSerialize();
        $obj["amount"] = round(doubleval($this->amount), 2);
        $obj["price"] = round(doubleval($this->price), 2);
        return $obj;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM wallet order BY id asc';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Wallet');
        Database::disconnect();
        $wallets=array();
        foreach ($items as $wallet){
            $wallets [] = WalletDetail::get($wallet->getId());
        }
        return $wallets !== false ? $wallets : null;
    }
    public static function get($id)
    {   $walletDetail = new WalletDetail();
        $amount =0;
        $price=0;
        $db = Database::connect();
        $sql = "SELECT * FROM purchase WHERE wallet_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchAll(PDO::FETCH_CLASS,'Purchase');  // ORM
        Database::disconnect();
        $wallet = Wallet::get($id);
        $walletDetail->setName($wallet->getName());
        $walletDetail->setCurrency($wallet->getCurrency());
        $walletDetail->setId($wallet->getId());
        foreach ($item as $detail){
            $amount += $detail->getAmount();
            $price += $detail->getPrice();
        }
        $walletDetail->setAmount($amount);
        $walletDetail->setPrice($price);
        //DEBUG
        //print_r($walletDetail);
        return $walletDetail !== false ? $walletDetail : null;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

}