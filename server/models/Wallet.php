<?php

require_once 'DatabaseObject.php';

class Wallet implements DatabaseObject, JsonSerializable
{
    private $id;
    private $name;
    private $currency;

    private $errors = [];

    public function validate()
    {
        return $this->validateCurrency() & $this->validateName();
    }
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO wallet (name, currency) values(?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->currency));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM wallet WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Wallet');
        Database::disconnect();
        return $item !== false ? $item : null;
    }
    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM wallet';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Wallet');

        Database::disconnect();

        return $items;
    }
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM wallet WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }
    /**
     * @inheritDoc
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE wallet set name = ?, currency = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->currency, $this->id));
        Database::disconnect();
    }
    public function jsonSerialize()
    {
        return [
            "id" => intval($this->id),
            "name" => $this->name,
            "currency" => $this->currency,
        ];
    }
    private function validateCurrency() {
        if (strlen($this->currency) == 0) {
            $this->errors['currency'] = "Währung ungültig";
            return false;
        } else if (strlen($this->currency) > 32) {
            $this->errors['currency'] = "Währung zu lang (max. 32 Zeichen)";
            return false;
        } else {
            unset($this->errors['currency']);
            return true;
        }
    }
    private function validateName() {
        if (strlen($this->name) == 0) {
            $this->errors['name'] = "Name ungültig";
            return false;
        } else if (strlen($this->name) > 10) {
            $this->errors['name'] = "Name zu lang (max. 32 Zeichen)";
            return false;
        } else {
            unset($this->errors['name']);
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}