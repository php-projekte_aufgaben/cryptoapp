app.component('wallet-list', {
    props: {
        wallets: {
            type: Array,
            required: true
        },
        assocp: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
        `<div class="wallet-list">
    <h3>Wallets: {{besitz +" €"}} {{win}}</h3>
      <ul>
        <li v-for="wallet in wallets">
          {{ wallet.name }}
          <br>
          Menge: {{wallet.amount}}
          <br>
          Wert:  {{currentWalletValue(wallet)}} {{win2(wallet)}}  
        </li>
      </ul>
    </div>
    `,
    data() {
        return {}
    },
    methods: {
        pricesLoaded() {
            return Object.keys(this.assocp).length > 0;
        },
        currentWalletValue(wallet) {
            if (this.pricesLoaded() && wallet.amount > 0) {
                return (this.assocp[wallet.currency] * wallet.amount).toFixed(2) + '€'
            } else {
                return (wallet.price * wallet.amount).toFixed(2) + '€'
            }
        },
        // Prototyp: Prozentausgabe der Veränderung
        win2(wallet) {
            if (this.pricesLoaded() && wallet.amount > 0) {
                const value = this.assocp[wallet.currency] * wallet.amount;
                //return '('+((value / wallet.price - 1) * 100).toFixed(1) + " %)"
                return '';
            } else {
                return '';
            }
        }
    },
    computed: {
        besitz() {
            var currentValue = 0;
            this.wallets.forEach(element => {
                currentValue += (this.assocp[element.currency] * element.amount);
            })
            return currentValue.toFixed(2);
        },
        // Prototyp: Prozentausgabe der Veränderung
        win() {
            if (this.pricesLoaded() && this.oldWalletValue > 0) {
                //return '( ' + ((this.besitz / this.oldWalletValue - 1) * 100).toFixed(1) + '%)';
                return '';
            } else {
                return ''
            }
        },
        oldWalletValue() {
            let value = 0;
            //this.purchases.forEach(element=> value += element.price);
            return value;
        }

    }
})
