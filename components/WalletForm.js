app.component('wallet-form', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        walletname: {
            type: Array,
            required: true
        },
        currency: {
            type: Array,
            required: true
        },
    },
    template:
    /*html*/
        `
  <form class="wallet-form"  @submit.prevent="onSubmit">
        <h3>Neue Wallet anlegen</h3>
        <div>
        <label for="walletName">Wallet Name:</label>
            <div>
                <input id="walletName" v-model="walletName">
            </div>
        <label for="name">Currency Name:</label>
            <div>
                <select v-model="walletCurrency">
                    <option v-for="price in prices" vbind:value="{currency: price.currencies}">
                        {{price.currencies}}
                    </option>
                </select>
            </div> 
    </div>
    
</div>
    <input class="button loader" type="submit" value="Neue Wallet">  
  </form>
`,

    data() {
        return {
            walletName: "",
            walletCurrency: "",
        }

    },
    methods: {
        onSubmit() {
            if (this.walletName === '' || this.walletCurrency === ``) {
                alert('Bitte füllen Sie alle Felder aus!')
                return
            }
            let newWallet = {
                name: this.walletName,
                currency: this.walletCurrency,
            }
            // Vue $emit lets us emit, or send, custom events from a child component to its parent.
            this.$emit("walletAdded", newWallet);
            console.log(newWallet);

            this.walletName = "";
            this.walletCurrency = "";
        }
    },
})

