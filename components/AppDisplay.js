app.component('app-display', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        wallets: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        },
        assocp: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/

    // Darstellung (VIEW)
        `<div class="wallet-display">
            <div class="wallet-container">
                <wallet-form @walletAdded="addWalletMain" :prices="prices" :name="walletName" :currency="walletCurrency" ></wallet-form>
                <purchase-form @purchaseAdded="addPurchaseMain" :prices="prices" :wallets="wallets" ></purchase-form>
                <wallet-list :wallets="wallets" :purchases="purchases" :assocp="assocp"></wallet-list>
            </div>
         </div>`
    ,
    data() {
        return {}
    },
    methods: {
        // Vue $emit lets us emit, or send, custom events from a child component to its parent.
        // To add validation, the event is assigned a function that receives the arguments passed
        // to the this.$emit call and returns a boolean to indicate whether the event is valid or not.
        addPurchaseMain(purchase) {
            this.$emit("super", purchase)
        },
        addWalletMain(newWallet) {
            this.$emit("super2", newWallet)
        }
    }
})