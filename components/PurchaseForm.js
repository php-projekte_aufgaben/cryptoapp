app.component('purchase-form', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        wallets: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
        `
  <form class="wallet-form" @submit.prevent="onSubmit">
        <h3>Cryptowährung kaufen</h3>
        <div>
        <label for="name">Cryptowährung:</label>
            <div>
                <select v-model="selectedCrypto">
                    <option v-for="price in prices" v-bind:value="{currency: price.currencies, value: price.values}">
                        {{price.currencies}}: {{ price.values}}€
                    </option>
                </select>
                <!-- DEBUG -->
<!--                {{selectedCrypto.value}}-->
<!--                {{selectedCrypto.currency}}-->
            </div>    
        </div>
        <label for="name">Wallet:</label>
           <div>
               <select v-model="selectedWallet">
                   <option v-for="wallet in wallets" v-bind:value="{id: wallet.id, name: wallet.name, currency: wallet.currency}">
                       {{wallet.currency}}: {{wallet.name}} 
                   </option>
               </select>
               <!-- DEBUG -->
<!--               {{selectedWallet.name}}-->
<!--               {{selectedWallet.id}}-->
<!--               {{selectedWallet.currency}}-->
           </div>
    <label for="review">Menge:</label>      
    <input id="inputMenge" v-model.number="inputMenge">
           {{inputMenge}}
    <label for="rating">Wert: {{wert}} €</label>
    <input class="button loader" type="submit" value="Kaufen">  
  </form>
`,

    data() {
        return {
            selectedCrypto: "",
            selectedWallet: "",
            inputMenge: ""
        }

    },
    methods: {
        onSubmit() {
            if (this.selectedCrypto == null || this.inputMenge === '' || this.inputMenge <= 0) {
                alert('Bitte füllen Sie alle Felder aus!')
                return
            }
            // Überprüfung der Übereinstimmung von Wallet und Cryptowährung
            if (this.selectedCrypto.currency === this.selectedWallet.currency) {
                //console.log("Hello World");

                // Datumsformat anpassen!
                var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -5).replace('T', ' ');

                // DEBUG
                //console.log(localISOTime)
                let purchase = {
                    date: localISOTime,
                    amount: this.inputMenge,
                    price: this.selectedCrypto.value * this.inputMenge,
                    wallet_id: this.selectedWallet.id
                }

                // Vue $emit lets us emit, or send, custom events from a child component to its parent.
                this.$emit("purchaseAdded", purchase);
                // console.log(purchase);
                this.selectedCrypto = "";
                this.selectedWallet = "";
                this.inputMenge = "";
            } else {
                alert('Cryptowährung und Wallet müssen übereinstimmen!')
            }
        }
    },
    computed: {
        wert() {
            if (this.selectedCrypto == null || this.inputMenge === '' || this.inputMenge < 0) {
                return 0
            } else {
                return this.selectedCrypto.value * this.inputMenge;
            }
        }
    }
})

