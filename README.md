# Cryptoapp

# Inhaltsverzeichnis

- [Cryptoapp](#cryptoapp)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Problemstellung](#problemstellung)
  - [Die Situation](#die-situation)
  - [Dein Ziel](#dein-ziel)
  - [Deine Zielgruppe](#deine-zielgruppe)
  - [Das erwartete Produkt](#das-erwartete-produkt)
- [Klassendiagramm / Ordnerstruktur](#klassendiagramm--ordnerstruktur)
- [Mockup / Wireframe](#mockup--wireframe)
- [Funktionen der REST-Schnittstelle](#funktionen-der-rest-schnittstelle)
  - [Alle Käufe auslesen (sortiert nach Datum)](#alle-käufe-auslesen-sortiert-nach-datum)
  - [Alle Käufe gruppiert nach Cryptowährung auslesen (Zusammenfassung)](#alle-käufe-gruppiert-nach-cryptowährung-auslesen-zusammenfassung)
  - [Neuen Kauf eintragen](#neuen-kauf-eintragen)
- [CryptoApp Extend (für Sehr Gut und Gut)](#cryptoapp-extend-für-sehr-gut-und-gut)
  - [Das erwartete Produkt](#das-erwartete-produkt-1)
- [Klassendiagramm](#klassendiagramm)
- [Mockup / Wireframe](#mockup--wireframe-1)
- [Funktionen der REST-Schnittstelle](#funktionen-der-rest-schnittstelle-1)
  - [Alle Käufe auslesen (sortiert nach Datum)](#alle-käufe-auslesen-sortiert-nach-datum-1)
  - [Einzelnen Kauf auslesen inkl. Wallet-Objekt (per ID)](#einzelnen-kauf-auslesen-inkl-wallet-objekt-per-id)
  - [Neuen Kauf eintragen](#neuen-kauf-eintragen-1)
  - [Alle Wallets auslesen (Zusammenfassung der Käufe)](#alle-wallets-auslesen-zusammenfassung-der-käufe)
  - [Einzelnes Wallet auslesen (Zusammenfassung der Käufe)](#einzelnes-wallet-auslesen-zusammenfassung-der-käufe)
  - [Alle Käufe eines Wallets auslesen](#alle-käufe-eines-wallets-auslesen)
- [VueJS Basics](#vuejs-basics)
  - [Deklaratives Rendern](#deklaratives-rendern)
- [Implementierung der Klassen](#implementierung-der-klassen)
  - [Main Js](#main-js)
  - [AppDisplay Js](#appdisplay-js)
    - [Validierungen](#validierungen)
- [Testprotokoll](#testprotokoll)
  - [Cryptowährung kaufen](#cryptowährung-kaufen)
  - [Fehlerprüfung bei nicht ausgefüllten Feldern bei Cryptokauf](#fehlerprüfung-bei-nicht-ausgefüllten-feldern-bei-cryptokauf)
  - [Fehlerprüfung Cryptowährung zu falscher Wallet hinzufügen](#fehlerprüfung-cryptowährung-zu-falscher-wallet-hinzufügen)
  - [Neue Wallet anlegen](#neue-wallet-anlegen)
  - [Fehlerprüfung bei nicht ausgefüllten Feldern bei neuer Wallet](#fehlerprüfung-bei-nicht-ausgefüllten-feldern-bei-neuer-wallet)
<br><br>


# Problemstellung

## Die Situation

Du sollst für eure Kunden eine Webanwendung mit Vue.js (Frontend) zum Kaufen von Cryptowährungen erstellen. Alle Daten werden über eine REST-SChnittstelle (Backend) übertragen. Teile der Schnittstelle und des Backends wurden bereits entwickelt. Diese kannst du als Grundlage für deine Implementierung verwenden.

## Dein Ziel

Dein Chef hat dich beauftragt einen Prototyp zu erstellen. Das Vue.js Frontend soll den Kauf von Cryptowährungen und das Anzeigen der getätigten Käufe (inkl. aktuellem Wert & prozentuellem Gewinn) ermöglichen. Die REST-API arbeitet wieder darstellungsunabhängig im JSON-Format.

## Deine Zielgruppe 

Die Applikation soll zukünftig eure Flagship-App werden und von allen Kunden verwendet werden. 

## Das erwartete Produkt 

Funktionen des einfachen Prototyps.
Der Prototyp sollte mindestens folgendes umfassen: 

- Model und REST-Schnittstelle zur Datenabfrage und Modifikation getätigter Käufe
- Implementierung mittels Vue.js Anwendung
- Anbindung der bitpanda-API zur Abfrage aktueller Kurse https://api.bitpanda.com/v1/ticker 
    - Hinweis: Die Abfrage der Kursdaten via JS in der Vue.js Applikation ist nicht optimal, da der User diese modifizieren kann! Für produktive Anwendung bitte im Gedächtnis behalten!

# Klassendiagramm / Ordnerstruktur  

![Ordnerstruktur](images/ordnerstruktur.png) 

# Mockup / Wireframe 

![Mockup1](images/mockup1.png) 

# Funktionen der REST-Schnittstelle 

Alle CRUD-Funktionen für Käufe

## Alle Käufe auslesen (sortiert nach Datum)

GET http://localhost/php43-crypto/server/api/purchase

```json
Ergebnis:
[
    {
        "id": 1,
        "date": "2021-02-01 09:07:19",
        "amount": 0.01,
        "price": 25000,
        "currency": "BTC"
    },
    {
        "id": 2,
        "date": "2021-01-25 19:07:19",
        "amount": 0.01,
        "price": 20000,
        "currency": "BTC"
    },
    …
]
``` 
*Hinweis:* Price entspricht dem Kurswert zum Kaufdatum. 

## Alle Käufe gruppiert nach Cryptowährung auslesen (Zusammenfassung) 

GET http://localhost/php43_crypto/server/api/purchase/currency/BTC

```json
Ergebnis:
[
    {
        "currency": "BTC",
        "amount": 0.02,
        "price": 450.0
    },
    …
]
```
*Hinweis:* 
- amount ist die Summe alle Mengen einer gekauften Währung
- price ist die SUmme aller Kaufpreise (Kurswert am Kaufdatum * gekaufte Menge)
- Die Angabe der Wähung BTC ist optional

## Neuen Kauf eintragen 

POST http://localhost/php43_angabe/server/api/purchase
Content-Type: application/json

```json
{"date":"2021-01-27 12:03:19","currency":"ETH","amount":0.3,"price":500}
``` 

# CryptoApp Extend (für Sehr Gut und Gut)

## Das erwartete Produkt

Funktionen des erwarteten Prototyps

Der Prototyp sollte mindestens folgendes umfassen:

- Einführung einer Wallet-Klasse (Geldbörsen für bestimmte Cryptowährungen)
    - 1:N Beziehung zu Käufe, jeder Kauf wird einer Wallet zugeordnet
    - Model und REST-Schnittstelle zur Datenabfrage und Modifikation von Wallets
  - Erweiterung der Vue.js Anwendung zur Auswahl des Zielwallets eines Kaufs
  - Einführung einer WalletDetail-Subklasse für die Abfrage von gruppierten Kaufinformationen (Zusammenfassung)

# Klassendiagramm 

![Klassendiagramm](images/klassendiagramm.png) 

# Mockup / Wireframe 

![Mockup2](images/mockup2.png)

# Funktionen der REST-Schnittstelle

Alle CRUD-Funktionen für Käufe
CUD-Funktionen für Wallets, R-Funktionen für WalletDetail

## Alle Käufe auslesen (sortiert nach Datum)

GET http://localhost/php43_angabe/server/api/purchase

Ergebnis
```json
[
    {
        "id": 19,
        "date": "2021-02-12 08:00:12",
        "amount": 0.6,
        "price": 1461.01,
        "wallet_id": 2
    },
    {
        "id": 2,
        "date": "2021-01-25 19:07:19",
        "amount": 0.01,
        "price": 20000,
        "wallet_id": 1
    },
    …
]
``` 

## Einzelnen Kauf auslesen inkl. Wallet-Objekt (per ID)

GET http://localhost/php43_angabe/server/api/purchase/2

Ergebnis
```json
{
    "id": 2,
    "date": "2021-01-25 19:07:19",
    "amount": 0.01,
    "price": 20000,
    "wallet_id": 1,
    "wallet": {
        "id": 1,
        "name": "Bitcoins",
        "currency": "BTC"
    }
}
```

## Neuen Kauf eintragen 

POST http://localhost/php43_angabe/server/api/purchase
Content-Type: application/json

```json
{"date":"2021-01-27 12:03:19","wallet_id":"2","amount":0.3,"price":500}
```

## Alle Wallets auslesen (Zusammenfassung der Käufe)

GET http://localhost/php43_angabe/server/api/wallet

Ergebnis
```
[
    {
        "id": 1,
        "name": "Bitcoins",
        "currency": "BTC",
        "amount": 0.05,
        "price": 1624.8584999999998
    },
    {
        "id": 2,
        "name": "Ethereums",
        "currency": "ETH",
        "amount": 1.3,
        "price": 1618.4279999999999
    },
    …
]
```

*Hinweise:* 
- amount ist die Summe aller Mengen einer gekauften Währung
- price ist die Summe aller Kaufpreise (Kurswert am Kaufdatum * gekaufte Menge)

## Einzelnes Wallet auslesen (Zusammenfassung der Käufe) 

GET http://localhost/php43_angabe/server/api/wallet/2

Ergebnis
```json
{
    "id": 2,
    "name": "Ethereums",
    "currency": "ETH",
    "amount": 1.3,
    "price": 1618.4279999999999
}
``` 
*Hinweise:*
- amount ist die Summe aller Mengen einer gekauften Währung
- price ist die SUmme alle Kaufpreise (Kurswert am Kaufdatum * gekaufte Menge)

## Alle Käufe eines Wallets auslesen

GET http://localhost/php43_angabe/server/api/wallet/2/purchase

Ergebnis
```json
[
    {
        "id": 20,
        "date": "2021-01-27 12:03:19",
        "amount": 0.3,
        "price": 500,
        "wallet_id": 2
    },
    {
        "id": 19,
        "date": "2021-02-12 08:00:12",
        "amount": 0.6,
        "price": 1461.01,
        "wallet_id": 2
    },
    …
]
```
<br><br>

# VueJS Basics

## Deklaratives Rendern

DOM = Document Object Model!

Deklaratives Rendering = Mithilfe einer HTML-erweiternden Vorlagesyntax können wir basierend auf dem JavaScript-Status beschreiben, wie der HTML-Code aussehen soll. Bei ändereungen des Statuses wird der HTML-Code automatisch aktualisiert.

Single File Component (SFC) = Ein SFC ist ein wiederverwendbarer, in sich geschlossener Codeblock, der HTML, CSS und JavaScript kapselt, die zusammengehören und in eine .vueDatei geschrieben sind.

Zustände, die bei Änderung Aktualisierungen auslösen können, gelten als reactiv.


Beispiele:

Mustache-Syntax = {{message}} (nur zur Textinterpolation = Veränderung literarischer Texte)

<br>

Um ein Attribut an einen dynamischen Wert zu binden, verwenden wir die **v-bind** Direktive.
**bind**wird nur einmal aufgerufen, wenn die Direktive zum ersten Mal an das Element gebunden wird. Hier können Sie einmalige Einrichtungsarbeiten durchführen.

```js
<div v-bind:id="dynamicId"></div>
```
Ähnlich wie Textinterpolationen sind Direktivenwerte JavaScript-Ausdrücke, die Zugriff auf den Zustand der Komponente haben.

Der Teil nach dem Doppelpunkt **( :id)** ist das **Argument** der Direktive. Hier wird das Attribut des Elements mit der Eigenschaft aus dem Status der Komponente **id** synchronisiert **.dynamicId**

Da v-bindes so häufig verwendet wird, hat es eine dedizierte Abkürzungssyntax:

```js
<div :id="dynamicId"></div>
```

<br>

Die **v-on** Direktive wird verwendet um DOM-Ereignisse zu reagieren und Js auszuführen, wenn diese ausgelöst werden. Eine alternative und auch bessere Schreibweise ist das **@** Zeichen.

```js
 v-on:click="Demo"
```

<br>

Die **v-model** Direktive wird verwendet, um bidirektionale Datenbindungen für Formulareingaben, Textbereiche und ausgewählte Elemente zu erstellen.
**Automatische wahl der richtigen Methode zum aktualisieren des Elements auf Basis des Eigabetyps**.
**v-model** verwendet intern unterschiedliche Eigenschaften.

```js
v-model.number="inputMenge"
```
<br>

Mit **@submit.prevent** wird beim Submit-Button die Anfrage an den Server verhindert. (Durch Ajax wird dies nicht benötigt) bzw. Verhindert das standartmäßige Verhalten!

```js
@submit.prevent="onSubmit"
```

<br>

# Implementierung der Klassen


## Main Js

Der **mounted()** Hook ist der am häufigsten verwendete Lifecircle-Hook in Vue. Er wird aufgerufen, wenn die Komponente zum DOM hinzugefügt wird. Häufigste verwendung: Um eine HTTP-Anforderung zum Abrufen von Daten zu senden, die die Komponente dann rendert.

```js
   },
    mounted() {
        this.loadPricesAssoc()
        this.loadPrices()
        this.loadWallets()
        this.loadPurchases()
    },
```

**Angabe zu den Methoden()**
Zuerst wird mit axios.get die angegebenen Daten via APi geholt und anschließend wird mit **then((response)** der weitere Verlauf bestimmt.
Das Schlüsselwort **let** deklariert eine Variable im Gültigkeitsbereich des lokalen Blocks. Optional wird die Variable mit einem Wert initialisiert.
In unserem Fall wird ein neues **Array** in die Variable **prices** gespeichert. In die Variable **keys** wird mit hilfe der **Object.keys** Methode ein Arry aus Stringlitereren gespecihert welches und als **Schlüssel**, das wir dies in ein Assoziatives Array speichern. Das Gleiche gilt für **Object.values**. Der einzige unterschied besteht darin das values ein Array der eigenen aufzählbaren Eigenschaftswerte eines gegebenen Objekts in der gleichen Reihenfolge zurück gibt, wie sie von einer for-Schleife geliefert wird. 

```js
        loadPrices() {
            axios.get("https://api.bitpanda.com/v1/ticker").then(
                (response) => {
                    let prices =[];
                    let keys = Object.keys(response.data);
                    let values = Object.values(response.data);
                    let europrices = [];
                    values.forEach( value => {
                            europrices.push(value.EUR)
                        }
                    )
                    let obj = {};
                    for(let i = 0; i < keys.length; i++){
                        let test = {currencies: keys[i], values: europrices[i]};
                        prices.push(test);
                    }
                    this.prices = prices;
                })
```

Bei loadPricesAssoc ist ein wichtiger Bestandteil die **Object.entries** Methode. Die Object.entries()-Methode gibt ein Array der eigenen aufzählbaren Eigenschafts-[Schlüssel, Wert]-Paare eines gegebenen Objekts zurück.

```js
 loadPricesAssoc() {
            axios.get("https://api.bitpanda.com/v1/ticker").then(response => {
                let prices = [];
                for (const [currency, value] of Object.entries(response.data)) {
                    prices[currency] = parseFloat(value.EUR)
                }
                this.assocp = prices
                //console.log(this.assocp['ETH']);
            })
```


Axios wird verwendet um über eine API die gewünschten Daten zu holen.

```js
axios.get("https://api.bitpanda.com/v1/ticker")
```

<br>

## AppDisplay Js 

Die Javascript Klasse AppDisplay wird verwendet um die Inhalte darzustellen.

Am Anfagn wird mit **app.component** eine Vue-Komponente "registriert" werden, damit Vue weiß wo sich die Implemntierung befindet. Die **app.component** Methode ist die Globale Registrierung und dadurch können Komponenten global in der aktuellen Vue-Anwendung verfügbar gemacht werden.

Prop Typen in unserem Fall Arrays deren verfügbarkeit verpflichtend ist.
Als Template wird HTML verwendet.


```js
app.component('app-display', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        wallets: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        },
        assocp: {
            type: Array,
            required: true
        }
    },
    template:
```
<br>

**Wichtig!**

Wie oben genannt wird die **v-on** in unseren Fall die **@** Direktive verwendet, um mit einem Event Listener auf DOM-Events zu reagieren. Beim Auslösen des Events wird der angegebene Js-Code ausgeführt. **Die Art des Events wird hinter einem Doppelpunkt angegeben**.

```html
  `<div class="wallet-display">
            <div class="wallet-container">
                <wallet-form @walletAdded="addWalletMain" :prices="prices" :name="walletName" :currency="walletCurrency" ></wallet-form>
                <purchase-form @purchaseAdded="addPurchaseMain" :prices="prices" :wallets="wallets" ></purchase-form>
                <wallet-list :wallets="wallets" :purchases="purchases" :assocp="assocp"></wallet-list>
            </div>
         </div>`
```

<br>

Die Vue Methode ist ein Objekt, das der Vue-Instanz zugeordnet ist. Funktionen werden innerhalb des Methodenobjekts definiert.

```
 methods: {]
```

<br>

Mit Vue $emit können benutzerdefinierte Ereignisse von einer untergeordneten Komponente an ihre übergeordnete Komponente ausgeben oder senden.

```js
  this.$emit("super", purchase)
```


Data ist eine Funktion, die den reaktiven Zustand für die Komponenteninstanz zurückgibt.
Es wird erwartet, dass eine einfaches Js-Objekt zurückgegebne wird.

```js
   ,
    data() {
        return {}
    },
```


### Validierungen

Sämtliche Validierungen der Js Klassen.
Aufgrund zahlreicher Validierungsforemn in den vorherigen Projekten ist die spezifische erläuterung hier nicht mehr notwendig.

Computed beinhaltet Funktionen welche keine Parameter besitzen dürfen und die Retunierten Werte werden temporär im Chache gespeichert.
Dadurch wird beim erneuten Laden der Seite keine neue berechnung durchgeführt, sondern der gespeicherte Wert wird übernommen.


```js
 computed: {
        wert() {
            if (this.selectedCrypto == null || this.inputMenge === '' || this.inputMenge < 0) {
                return 0
            } else {
                return this.selectedCrypto.value * this.inputMenge;
            }
        }
    }
```


```js
 onSubmit() {
            if (this.walletName === '' || this.walletCurrency === ``) {
                alert('Bitte füllen Sie alle Felder aus!')
                return
            }
            let newWallet = {
                name: this.walletName,
                currency: this.walletCurrency,
            }
            // Vue $emit lets us emit, or send, custom events from a child component to its parent.
            this.$emit("walletAdded", newWallet);
            //console.log(newWallet);

            this.walletName = "";
            this.walletCurrency = "";
 }
},
```

**Object.keys()** liefert ein Array, dessen Elemente Strings sind, welche die aufzählbaren Eigenschaften des Objekts respräsentieren. Die Reihenfolge der Eigenschaften ist die selbe wie bei einer for-each Schleife über das Objekt.

```js
  methods: {
        pricesLoaded() {
            return Object.keys(this.assocp).length > 0;
        },
        currentWalletValue(wallet) {
            if (this.pricesLoaded() && wallet.amount > 0) {
                return (this.assocp[wallet.currency] * wallet.amount).toFixed(2) + '€'
            } else {
                return (wallet.price * wallet.amount).toFixed(2) + '€'
            }
        },
        // Prototyp: Prozentausgabe der Veränderung
        win2(wallet) {
            if (this.pricesLoaded() && wallet.amount > 0) {
                const value = this.assocp[wallet.currency] * wallet.amount;
                //return '('+((value / wallet.price - 1) * 100).toFixed(1) + " %)"
                return '';
            } else {
                return '';
            }
        }
    },
    computed: {
        besitz() {
            var currentValue = 0;
            this.wallets.forEach(element => {
                currentValue += (this.assocp[element.currency] * element.amount);
            })
            return currentValue.toFixed(2);
        },
        // Prototyp: Prozentausgabe der Veränderung
        win() {
            if (this.pricesLoaded() && this.oldWalletValue > 0) {
                //return '( ' + ((this.besitz / this.oldWalletValue - 1) * 100).toFixed(1) + '%)';
                return '';
            } else {
                return ''
            }
        },
        oldWalletValue() {
            let value = 0;
            //this.purchases.forEach(element=> value += element.price);
            return value;
        }

    }
```




<br>

# Testprotokoll 

![TestStart](images/testStart.png) 

![TestStart2](images/testSTart2.png)

## Cryptowährung kaufen

![CryptoKaufen](images/cryptoKaufen.png)

![CryptoKaufen2](images/cryptoKaufen2.png)

![CryptoKaufen3](images/cryptoKaufen3.png)

![CryptoKaufen4](images/cryptoKaufen4.png)

## Fehlerprüfung bei nicht ausgefüllten Feldern bei Cryptokauf

![nichtAusgefüllt1](images/nichtAusgefüllt1.png)

![nichtAusfgefüllt2](images/nichtAusgefüllt2.png)

![nichtAusgefüllt3](images/nichtAusgefüllt3.png)

![nichtAusgefüllt4](images/nichtAusgefüllt4.png)

![nichtAusgefüllt5](images/nichtAusgefüllt5.png)

## Fehlerprüfung Cryptowährung zu falscher Wallet hinzufügen 

![falscheWallet1](images/falscheWallet1.png)

![falscheWallet2](images/falscheWallet2.png)

![falscheWallet3](images/falscheWallet3.png)

![falscheWallet4](images/falscheWallet4.png)

![falscheWallet5](images/falscheWallet5.png)

## Neue Wallet anlegen 

![walletAnlegen1](images/walletAnlegen1.png)

![walletAnlegen2](images/walletAnlegen2.png)

## Fehlerprüfung bei nicht ausgefüllten Feldern bei neuer Wallet 

![walletFehler1](images/walletFehler1.png)

![walletFehler2](images/walletFehler2.png)

